package com.example.practica0292_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnIMC: Button
    private lateinit var btnBorrar: Button
    private lateinit var btnTerminar: Button
    private lateinit var txtAlto: EditText
    private lateinit var txtKilos: EditText
    private lateinit var txtIMC2: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar los objetos
        btnIMC = findViewById(R.id.btnCalcular)
        btnBorrar = findViewById(R.id.btnLimpiar)
        btnTerminar = findViewById(R.id.btnCerrar)
        txtAlto = findViewById(R.id.txtAltura)
        txtKilos = findViewById(R.id.txtPeso)
        txtIMC2 = findViewById(R.id.txtIMC)

        // Codificar el botón Calcular
        btnIMC.setOnClickListener {
            // Tomar el texto de los EditText (texto escrito por el usuario)
            val txtAlturaU = txtAlto.text.toString()
            val txtPesoU = txtKilos.text.toString()

            if (txtAlturaU.isEmpty() || txtPesoU.isEmpty()) {
                // Requerir campos
                Toast.makeText(this@MainActivity, "Todos los campos son requeridos", Toast.LENGTH_LONG).show()
            } else {
                // Tomar los valores altura y peso
                val altura = txtAlto.text.toString().toDouble()
                val peso = txtKilos.text.toString().toDouble()

                // Validar que los valores no sean 0
                if (altura != 0.0 || peso != 0.0) {
                    // Se convierte la altura a metros
                    val alturaMetros = altura / 100
                    // Se calcula el IMC = peso / altura^2
                    val imc = peso / (alturaMetros * alturaMetros)

                    // Impresión del IMC
                    txtIMC2.text = "Su IMC es: $imc"
                } else {
                    Toast.makeText(this@MainActivity, "Introduzca valores mayores a 0", Toast.LENGTH_LONG).show()
                    btnBorrar.callOnClick()
                }
            }
        }

        btnBorrar.setOnClickListener {
            txtAlto.setText("")
            txtKilos.setText("")
            txtIMC2.text = "Su IMC es:"
        }

        btnTerminar.setOnClickListener { finish() }
    }
}
